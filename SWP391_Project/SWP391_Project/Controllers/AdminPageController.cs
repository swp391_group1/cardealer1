﻿using Microsoft.AspNetCore.Mvc;

namespace SWP391_Project.Controllers
{
    public class AdminPageController : Controller
    {
        public IActionResult AdminHomePage()
        {
            return View();
        }
    }
}
